#!/bin/bash
ODOO_VERSION="12.0";
case $1 in
    latest|7.0|8.0|9.0|10.0|11.0|12.0|13.0|14.0)
        ODOO_VERSION="$1";
        shift;
    ;;
esac

docker run --rm -d -p 80:8069 shaileshlathiya/odoo-ci:$ODOO_VERSION $@; 
